PYTHON = python3

all:

check:
	$(MAKE) -C tests
	(cd tests && $(PYTHON) test.py)

clean:
	$(MAKE) -C tests clean

.PHONY: all check clean
