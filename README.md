# objtree #

objtree shows the dependency tree of an object file.

It is very similar to lddtree (from pax-utils);
however, objtree uses `objdump` instead of `scanelf`, allowing it to read more file formats.
Notably, it works on PE files (e.g. Windows `.exe` and `.dll` files).


## Example usage ##

    > objtree avcodec-57.dll
    avcodec-57.dll => C:\ffmpeg\avcodec-57.dll
        ADVAPI32.dll => C:\WINDOWS\System32\ADVAPI32.dll
        KERNEL32.dll => C:\WINDOWS\System32\KERNEL32.dll
        msvcrt.dll => C:\WINDOWS\System32\msvcrt.dll
        USER32.dll => C:\WINDOWS\System32\USER32.dll
        avutil-55.dll => C:\ffmpeg\avutil-55.dll
            ADVAPI32.dll => C:\WINDOWS\System32\ADVAPI32.dll
            KERNEL32.dll => C:\WINDOWS\System32\KERNEL32.dll
            msvcrt.dll => C:\WINDOWS\System32\msvcrt.dll
        swresample-2.dll => C:\ffmpeg\swresample-2.dll
            KERNEL32.dll => C:\WINDOWS\System32\KERNEL32.dll
            msvcrt.dll => C:\WINDOWS\System32\msvcrt.dll
            avutil-55.dll => C:\ffmpeg\avutil-55.dll
                ADVAPI32.dll => C:\WINDOWS\System32\ADVAPI32.dll
                KERNEL32.dll => C:\WINDOWS\System32\KERNEL32.dll
                msvcrt.dll => C:\WINDOWS\System32\msvcrt.dll


## Limitations ##

objtree file format support is limited by whatever your version of objdump supports.
If objdump cannot read a particular file, you will get an error similar to

    objdump: ...: File format not recognized

If objtree only shows a dependency on `mscoree.dll`, it means your file is a .NET assembly.
.NET assembly dependencies are implemented at a level above the PE format which objdump (and thus objtree) cannot show.


## Requirements ##

objtree requires Python 3.5 and objdump (part of GNU binutils).