#!/usr/bin/env python3

# objtree - show tree of object file dependencies
# Copyright (c) 2014, 2016 Johannes Sasongko <sasongko@gmail.com>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


"""Show tree of object file dependencies.

`objtree` creates a dependency tree of .dll files, similarly to how `lddtree`
does it for .so files. Although `objtree` also works on .so (and .o) files,
currently it has no RPath support, which makes it unreliable when dealing with
.so files on platforms with RPath.

`objtree` requires `objdump`. It runs `objdump` to find the dependencies of the
target objects, then does it recursively on each dependency in order to
construct the tree.
"""


import os, re, subprocess, sys
from typing import AnyStr, Optional


dec = os.fsdecode
enc = os.fsencode
win32 = sys.platform == 'win32'


class StrStr(str):
	def __new__(cls, s:AnyStr=''):
		return str.__new__(cls, dec(s) if isinstance(s, bytes) else s)

class BytesStr(bytes):
	def __new__(cls, s:AnyStr=b''):
		return bytes.__new__(cls, enc(s) if isinstance(s, str) else s)

Str = StrStr if win32 else BytesStr

class Environ:
	def __getitem__(self, var:AnyStr) -> Str:
		if os.supports_bytes_environ:
			if isinstance(var, str):
				var = enc(var)
			return Str(os.environb[var])
		else:
			if isinstance(var, bytes):
				var = dec(var)
			return Str(os.environ[var])
	def get(self, var:AnyStr) -> Optional[Str]:
		try:
			return self[var]
		except KeyError:
			return None

environ = Environ()


DEP_RE = re.compile(br'^\s*(?:DLL Name:|NEEDED)\s*(.+?)\r?$', re.M)

HOST = environ.get('HOST')
OBJDUMP = environ.get('OBJDUMP')
if not OBJDUMP:
	if HOST:
		OBJDUMP = HOST + Str('-objdump')
	else:
		OBJDUMP = Str('objdump')

def _init_paths():
	global PATHS, SYS_PREFIXES
	paths = environ.get('PATH')
	if paths:
		PATHS = paths.split(Str(os.path.pathsep))
	else:
		PATHS = []
	if win32:
		SYS_PREFIXES = [environ['SystemRoot'].lower() + '\\system32\\']
	else:
		if HOST and b'mingw' in HOST:
			PATHS.insert(0, b'/usr/' + HOST + b'/bin')
		else:
			host = b'/' + HOST if HOST else b''
			PATHS.insert(0, b'/usr' + host + b'/lib')
			PATHS.insert(0, b'/usr/local' + host + b'/lib')
		SYS_PREFIXES = [b'/usr/local/lib', b'/usr/lib']

_init_paths()


depcache = {}


def find_in_paths(fname, paths=PATHS):
	for path in paths:
		fpath = os.path.join(path, fname)
		fpath = os.path.realpath(fpath)
		if os.path.isfile(fpath):
			break
	else:
		return None
	return fpath


def print_obj(obj, quiet_sys=True, objpath=None, parentdeps=None, ancestors=frozenset(), paths=None, level=0):
	"""
	obj: object location (can be absolute, relative, or (if level>1) in PATH)
	quiet_sys: whether to hide dependencies of system objects
	
	objpath: absolute location of object, if known
	ancestors: list of ancestor objpaths, for catching circular dependency
	parentdeps: if specified, the resolved objpath willl be added to this
	paths: paths to search for objects; automatically detected on level 0
	level: indentation level for printing
	"""
	if paths is None:
		paths = [os.path.split(obj)[0]] + PATHS
	if not objpath:
		if level == 0:
			objpath = os.path.realpath(obj)
		else:
			objpath = find_in_paths(obj, paths)
	if parentdeps is not None:
		parentdeps.append((obj, objpath))
	print('    ' * level, dec(obj), ' => ', dec(objpath or '?'), sep='', end='')
	if objpath in ancestors:
		print(' [CIRCULAR]')
		return
	else:
		print()
	if not objpath:
		return
	if quiet_sys:
		o = objpath.lower() if win32 else objpath
		if any(o.startswith(p) for p in SYS_PREFIXES):
			return
	
	ancestors = ancestors.union((objpath,))
	level += 1
	
	objs = depcache.get(objpath)
	if objs is None:
		try:
			out = subprocess.check_output([OBJDUMP, Str('-p'), objpath])
		except subprocess.CalledProcessError:
			return
		objs = depcache[objpath] = []
		for m in re.finditer(DEP_RE, out):
			objs.append((Str(m.group(1)), None))
	else:
		parentdeps = None  # Already known, no need to populate
	
	for obj, objpath in objs:
		print_obj(obj, quiet_sys=quiet_sys, objpath=objpath, parentdeps=parentdeps, ancestors=ancestors, paths=paths, level=level)


def print_objs(objs, quiet_sys):
	for obj in objs:
		print_obj(obj, quiet_sys=quiet_sys)


def main():
	import argparse
	parser = argparse.ArgumentParser(description="Show tree of object file dependencies.")
	parser.add_argument('files', metavar="FILE", nargs='+', help="object file to process")
	parser.add_argument('-q', '--quiet', action='store_true', default=True,
		help="disable printing dependencies of libraries inside system paths [default]")
	parser.add_argument('-Q', '--no-quiet', action='store_false', dest='quiet',
		help="enable printing dependencies of libraries inside system paths")
	args = parser.parse_args()
	print_objs(map(Str, args.files), quiet_sys=args.quiet)


if __name__ == '__main__':
	main()


# vi: noet sts=4 sw=4 ts=4 tw=79
