all: testá.exe

testá.exe: testá.c testá.lib
	$(CC) /Fe:$@ /nologo $(CFLAGS) $**

testá.lib: $*.dll

testá.dll: libtestá.c
	$(CC) /Fe:$@ /nologo /LD $(CFLAGS) $** /link /IMPLIB:$*.lib

clean:
	del *.exp *.dll *.lib *.obj
