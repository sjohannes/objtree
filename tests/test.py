#!/usr/bin/env python3
# vim: noet fileencoding=utf-8

import os
import re
import subprocess
import sys

def main():
	cwd = os.path.dirname(os.path.abspath(__file__))
	os.chdir(cwd)
	p = subprocess.run([sys.executable, '../objtree.py', 'testá.exe'], stdout=subprocess.PIPE)
	out = os.fsdecode(p.stdout)
	expected = re.compile(
		r'testá\.exe => {cwd}{s}testá.exe(?:{n}|{n}.*{n})    testá.dll => {cwd}{s}testá.dll'
		.format(cwd=re.escape(cwd), s=re.escape(os.sep), n=re.escape(os.linesep)), re.S)
	assert expected.match(out), "expected: %r -- actual: %r" % (expected, out)
	print("ok")

if __name__ == '__main__':
	main()
